import random
import math
import seaborn as sns
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib import patches

def main():
    sns.set(style='darkgrid')

    # seed the pseudo-random generator
    random.seed()
    
    # program environments
    approx = np.empty(shape=(0, 2))
    circle = np.empty(shape=(0, 2))
    iterations = 100000
    inside_circle = 0
    outside_circle = 0
    window = 100

    # fig = plt.figure()
    # ax = plt.axes(xlim=(0, window), ylim=(0, 7))
    # line, ref = ax.plot([], [], [], [], lw=2)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), num='Monte Carlo - Pi Approximation')
    fig.suptitle('Monte Carlo - Pi Approximation', fontsize=18)

    ax1.set_ylim(0, 7)
    ax1.set_xlim(0, window)
    ax1.set_xlabel('no. points')
    ax1.set_ylabel('pi approximation')
    line, ref = ax1.plot([], [], [], [], lw=2)
    pi_text = ax1.text(0.02, 0.95, '', transform=ax1.transAxes)
    pi_error_text = ax1.text(0.02, 0.87, '', transform=ax1.transAxes)

    ax2.set_ylim(0, 1)
    ax2.set_xlim(0, 1)
    ax2.set_xlabel('x')
    ax2.set_ylabel('y')
    scatter, = ax2.plot([], [], 'o', ms=1)
    circ = patches.Circle((0, 0), radius=1, linewidth=1.5, edgecolor='r', facecolor='none')
    ax2.add_patch(circ)

    def init():
        nonlocal line
        line.set_data([], [])
        return line,

    def animate(i):
        nonlocal inside_circle, outside_circle, approx, circle, window, line, ref, ax1, scatter, ax2
        nonlocal pi_text, pi_error_text
        x_gen = random.random()
        y_gen = random.random()

        # count the points inside/outside of the semi-circle.
        if math.sqrt(x_gen ** 2 + y_gen ** 2) <= 1.0:
            inside_circle += 1
        else:
            outside_circle += 1

        # approximate the pi - it has to be multiplied by 4 because these points only represent the 1st quadrant
        pi_approx = (inside_circle / (outside_circle + inside_circle)) * 4
        
        # create numpy arrays for the dots and for the pi approximation
        circle = np.append(circle, [[x_gen, y_gen]], axis=0)
        approx = np.append(approx, [[inside_circle + outside_circle, pi_approx]], axis=0)

        # if we have filled all the figure with points then side to the right
        if approx.shape[0] >= window:
            # draw the pi approximation values
            row = approx.shape[0]
            line.set_data(approx[row-window:row,0], approx[row-window:row,1])
            
            # set the limits on x axis
            ax1.set_xlim((row-window, row))

            # calculate min and max value for approximated pi
            min_val = np.amin(approx[row-window:row,:], axis=0)[1]
            max_val = np.amax(approx[row-window:row,:], axis=0)[1]
            
            # set the limits on y axis
            magnitude = max_val - min_val
            ymid = (max_val + min_val) / 2
            ylim_up = ymid + magnitude * 15
            ylim_down = ymid - magnitude * 15
            ax1.set_ylim((ylim_down, ylim_up))

            # draw the pi reference line
            pi_line = np.full(window, math.pi)
            ref.set_data(approx[row-window:row,0], pi_line)

            # text
            pi_text.set_text('pi approx = {:.6f}'.format(approx[row-1,1]))
            pi_error_text.set_text('error = {:.6f}'.format(math.pi - approx[row-1,1]))

        # otherwise just keep on adding points (it only happens at the beggining)
        else:
            # draw the pi approximation values
            line.set_data(approx[:,0], approx[:,1])
            
            # draw the pi reference line
            row = approx.shape[0]
            pi_line = np.full(row, math.pi)
            ref.set_data(approx[:,0], pi_line)

            # text
            pi_text.set_text('pi approx = {:.6f}'.format(approx[row-1,1]))
            pi_error_text.set_text('error = {:.6f}'.format(math.pi - approx[row-1,1]))

        # draw the scatter plot with the semi-circle in it
        scatter.set_data(circle[:,0], circle[:,1])

        return line, ref, scatter

    anim = animation.FuncAnimation(fig, animate, frames=iterations, init_func=init, interval=10, repeat=True, blit=False)
    plt.show()

    # and display the results
    # print('Pi Approx = {:6.3f} | Error = {:6.3f}'.format(pi_approx, math.pi - pi_approx))

if __name__ == "__main__":
    main()